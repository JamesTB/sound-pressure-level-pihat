EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Status LEDs and Button"
Date "2019-02-28"
Rev "v1.0"
Comp "EEE3088F Group 59"
Comment1 "This Schematic is licensed under MIT Open Source License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R16
U 1 1 60B869FB
P 1450 2900
F 0 "R16" H 1509 2946 50  0000 L CNN
F 1 "10k" H 1509 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1450 2900 50  0001 C CNN
F 3 "~" H 1450 2900 50  0001 C CNN
	1    1450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2600 1450 2800
Wire Wire Line
	1450 3000 1450 3200
Wire Wire Line
	1450 3200 1800 3200
$Comp
L Switch:SW_Push SW1
U 1 1 60B8A655
P 2000 3200
F 0 "SW1" H 2000 3485 50  0000 C CNN
F 1 "SW_Push" H 2000 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H7.3mm" H 2000 3400 50  0001 C CNN
F 3 "~" H 2000 3400 50  0001 C CNN
	1    2000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3200 2500 3200
Wire Wire Line
	2500 3200 2500 3400
$Comp
L power:GND #PWR04
U 1 1 60B8C85C
P 2500 3400
F 0 "#PWR04" H 2500 3150 50  0001 C CNN
F 1 "GND" H 2505 3227 50  0000 C CNN
F 2 "" H 2500 3400 50  0001 C CNN
F 3 "" H 2500 3400 50  0001 C CNN
	1    2500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3200 1250 3200
Connection ~ 1450 3200
Wire Wire Line
	1150 1150 950  1150
Wire Wire Line
	950  1150 950  1250
$Comp
L power:GND #PWR02
U 1 1 60B9285E
P 950 1250
F 0 "#PWR02" H 950 1000 50  0001 C CNN
F 1 "GND" H 955 1077 50  0000 C CNN
F 2 "" H 950 1250 50  0001 C CNN
F 3 "" H 950 1250 50  0001 C CNN
	1    950  1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1150 1850 1150
Wire Wire Line
	1550 1350 1850 1350
$Comp
L Device:R_Small R18
U 1 1 60B9744C
P 1950 950
F 0 "R18" V 1754 950 50  0000 C CNN
F 1 "0.25k" V 1845 950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1950 950 50  0001 C CNN
F 3 "~" H 1950 950 50  0001 C CNN
	1    1950 950 
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 60B97B4F
P 1950 1150
F 0 "R19" V 1900 950 50  0000 C CNN
F 1 "0.06k" V 1845 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1950 1150 50  0001 C CNN
F 3 "~" H 1950 1150 50  0001 C CNN
	1    1950 1150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 60B980ED
P 1950 1350
F 0 "R17" V 2050 1350 50  0000 C CNN
F 1 "0.06k" V 1845 1350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1950 1350 50  0001 C CNN
F 3 "~" H 1950 1350 50  0001 C CNN
	1    1950 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 1150 2350 1150
Wire Wire Line
	2050 1350 2350 1350
Wire Wire Line
	2050 950  2350 950 
$Comp
L Device:LED D3
U 1 1 60BA74FE
P 1800 5750
F 0 "D3" V 1839 5632 50  0000 R CNN
F 1 "LED" V 1748 5632 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1800 5750 50  0001 C CNN
F 3 "~" H 1800 5750 50  0001 C CNN
	1    1800 5750
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 60BA79A0
P 2250 5750
F 0 "D4" V 2289 5632 50  0000 R CNN
F 1 "LED" V 2198 5632 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2250 5750 50  0001 C CNN
F 3 "~" H 2250 5750 50  0001 C CNN
	1    2250 5750
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 60BA7CAE
P 3150 5800
F 0 "D6" V 3189 5682 50  0000 R CNN
F 1 "LED" V 3098 5682 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 3150 5800 50  0001 C CNN
F 3 "~" H 3150 5800 50  0001 C CNN
	1    3150 5800
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 60BA884B
P 1400 5750
F 0 "D2" V 1439 5632 50  0000 R CNN
F 1 "LED" V 1348 5632 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1400 5750 50  0001 C CNN
F 3 "~" H 1400 5750 50  0001 C CNN
	1    1400 5750
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 60BA8CF4
P 3550 5800
F 0 "D7" V 3589 5682 50  0000 R CNN
F 1 "LED" V 3498 5682 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 3550 5800 50  0001 C CNN
F 3 "~" H 3550 5800 50  0001 C CNN
	1    3550 5800
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D5
U 1 1 60BA8EF9
P 2700 5750
F 0 "D5" V 2739 5632 50  0000 R CNN
F 1 "LED" V 2648 5632 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2700 5750 50  0001 C CNN
F 3 "~" H 2700 5750 50  0001 C CNN
	1    2700 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1400 5900 1400 6200
Wire Wire Line
	1400 6200 1800 6200
Wire Wire Line
	3550 6200 3550 5950
Wire Wire Line
	3150 5950 3150 6200
Connection ~ 3150 6200
Wire Wire Line
	2700 5900 2700 6200
Connection ~ 2700 6200
Wire Wire Line
	2700 6200 3150 6200
Wire Wire Line
	2250 5900 2250 6200
Connection ~ 2250 6200
Wire Wire Line
	1800 5900 1800 6200
Connection ~ 1800 6200
Wire Wire Line
	1800 6200 2250 6200
Wire Wire Line
	2250 6200 2700 6200
$Comp
L Device:LED D1
U 1 1 60BBFEEA
P 1000 5750
F 0 "D1" V 1039 5632 50  0000 R CNN
F 1 "LED" V 948 5632 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1000 5750 50  0001 C CNN
F 3 "~" H 1000 5750 50  0001 C CNN
	1    1000 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 5900 1000 6200
Wire Wire Line
	1000 6200 1400 6200
Connection ~ 1400 6200
Wire Wire Line
	2250 6200 2250 6500
$Comp
L power:GND #PWR01
U 1 1 60BC52D2
P 2250 6500
F 0 "#PWR01" H 2250 6250 50  0001 C CNN
F 1 "GND" H 2255 6327 50  0000 C CNN
F 2 "" H 2250 6500 50  0001 C CNN
F 3 "" H 2250 6500 50  0001 C CNN
	1    2250 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 60BC7A17
P 1000 5500
F 0 "R9" H 941 5454 50  0000 R CNN
F 1 "50" H 941 5545 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1000 5500 50  0001 C CNN
F 3 "~" H 1000 5500 50  0001 C CNN
	1    1000 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 60BC8271
P 1400 5500
F 0 "R10" H 1341 5454 50  0000 R CNN
F 1 "50" H 1341 5545 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1400 5500 50  0001 C CNN
F 3 "~" H 1400 5500 50  0001 C CNN
	1    1400 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 60BC84C3
P 1800 5500
F 0 "R11" H 1741 5454 50  0000 R CNN
F 1 "50" H 1741 5545 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 1800 5500 50  0001 C CNN
F 3 "~" H 1800 5500 50  0001 C CNN
	1    1800 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 60BC8896
P 2250 5500
F 0 "R12" H 2191 5454 50  0000 R CNN
F 1 "50" H 2191 5545 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 2250 5500 50  0001 C CNN
F 3 "~" H 2250 5500 50  0001 C CNN
	1    2250 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 60BC8AC3
P 2700 5500
F 0 "R13" H 2641 5454 50  0000 R CNN
F 1 "50" H 2641 5545 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 2700 5500 50  0001 C CNN
F 3 "~" H 2700 5500 50  0001 C CNN
	1    2700 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 60BC8D6E
P 3150 5550
F 0 "R14" H 3091 5504 50  0000 R CNN
F 1 "50" H 3091 5595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 3150 5550 50  0001 C CNN
F 3 "~" H 3150 5550 50  0001 C CNN
	1    3150 5550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 60BC9159
P 3550 5550
F 0 "R15" H 3491 5504 50  0000 R CNN
F 1 "50" H 3491 5595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 3550 5550 50  0001 C CNN
F 3 "~" H 3550 5550 50  0001 C CNN
	1    3550 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 6200 3550 6200
Wire Wire Line
	1000 5400 1000 5000
Wire Wire Line
	1400 5400 1400 5000
Wire Wire Line
	1800 5400 1800 5000
Wire Wire Line
	2250 5400 2250 5000
Wire Wire Line
	2700 5400 2700 5000
Wire Wire Line
	3150 5450 3150 5000
Wire Wire Line
	3550 5450 3550 5000
Text Notes 3800 4900 0    50   ~ 0
Here since I could not find the kiCad model,
Text Notes 3800 5000 0    50   ~ 0
I decided to actually implent the LEDs myself on the design.
Text Notes 3800 5100 0    50   ~ 0
The idea was to use seven 3mm LEDs each with a max voltage drop of 2.6V
Text Notes 3800 5200 0    50   ~ 0
and a minimum voltage drop of 1.9V.
Text Notes 3800 5300 0    50   ~ 0
The 3 mm size of the LEDs is to allow for a small footprint
$Comp
L Device:LED D12
U 1 1 60BA4C53
P 1400 950
F 0 "D12" H 1500 1050 50  0000 C CNN
F 1 "LED" H 1200 1000 50  0000 C CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1400 950 50  0001 C CNN
F 3 "~" H 1400 950 50  0001 C CNN
	1    1400 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D10
U 1 1 60BA5B10
P 1400 1150
F 0 "D10" H 1550 1200 50  0000 C CNN
F 1 "LED" H 1250 1200 50  0000 C CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1400 1150 50  0001 C CNN
F 3 "~" H 1400 1150 50  0001 C CNN
	1    1400 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D11
U 1 1 60BA65BD
P 1400 1350
F 0 "D11" H 1550 1400 50  0000 C CNN
F 1 "LED" H 1250 1400 50  0000 C CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1400 1350 50  0001 C CNN
F 3 "~" H 1400 1350 50  0001 C CNN
	1    1400 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 950  1850 950 
Wire Wire Line
	1150 1150 1150 950 
Wire Wire Line
	1150 950  1250 950 
Wire Wire Line
	1150 1150 1250 1150
Connection ~ 1150 1150
Wire Wire Line
	1150 1150 1150 1350
Wire Wire Line
	1150 1350 1250 1350
Text GLabel 1000 5000 1    50   Input ~ 0
GPIO6
Text GLabel 1400 5000 1    50   Input ~ 0
GPIO12
Text GLabel 1800 5000 1    50   Input ~ 0
GPIO13
Text GLabel 2250 5000 1    50   Input ~ 0
GPIO19
Text GLabel 2700 5000 1    50   Input ~ 0
GPIO20
Text GLabel 3150 5000 1    50   Input ~ 0
GPIO21
Text GLabel 3550 5000 1    50   Input ~ 0
GPIO25_GEN6
Text GLabel 1250 3200 0    50   Input ~ 0
GPIO5
Text GLabel 1450 2600 0    50   Input ~ 0
5V_Regulated
Text GLabel 2350 950  2    50   Input ~ 0
GPIO17_GEN0
Text GLabel 2350 1150 2    50   Input ~ 0
GPIO27_GEN1
Text GLabel 2350 1350 2    50   Input ~ 0
GPIO22_GEN2
$EndSCHEMATC
