To clone the Gitlab repo, go to the main page of the repository.\
There should be a blue icon that says clone on the top right side of the screen, click this button\
and choose to clone over http. By going to your gitbash terminal, you should be able to clone the\
repository by typing "git clone" followed by the url you have copied.\
\
Once cloned by going to the KiCAD file of the PCB Design folder, if you open the PCB design file,\
you should be able to fabricate a BOM from the menu by going:\
file>Fabrication Outputs>BOM File\
This file can also be found here: https://gitlab.com/JamesTB/sound-pressure-level-pihat/-/blob/master/PCB-design-folder/PCB-SPLDevice-uHAT-BOM.csv
