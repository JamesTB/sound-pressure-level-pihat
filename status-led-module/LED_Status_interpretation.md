# HOW The User SHould Interprete the LEDs
This document outlines a way the user shloud interprete the number of lit up LEDs. The number of lit 
up LEDs is used to indicate the noise level in an environment.

## Three or Less LEDs lit up
If the number of LEDs lit up is equal to three or less then that indicates that the sound level in the
particular room is proper for human hearing(3 LEDs lit up) or it's less.

## Four or more LEDs lit up
If the number of LEDs lit up by the raspberry pi is four or above then that indicates based on the mode
the device is operating in that the noise level is hign.

## The colours of the RGB LED
Based on the colour of the RGB LED the user can tell which mode the device is operating in. If it's red coloured,
then the device is operating in High Level Check mode. Should the LED be green coloured it indicates that the device
is operating in Low-Level check, while if it's blue it indicates that the device is operating in Talking Mic-Check mode.

